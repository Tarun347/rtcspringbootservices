package com.dci.rest.dao.elastic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.exception.DocubuilderException;

@Component
@Repository
public class ElasticServerKafkaDAO {
	
	private Logger developerLog = Logger.getLogger(ComponentDAO.class);
	
	@Value("${ELASTIC_SERVER_KAFKA_PRODUCER}" ) private String kafkaElasticProducer;
	
	@Value("${REQUEST_TIME_OUT}" ) String reqTimeOut;
	
	@Autowired
	private Environment env;
	
	
	public String kafkaProducerNotifyComponentUpdation(String user,String client,String elementInstanceId)  {
			      MDC.put("category","com.dci.rest.bdo.elastic");
				  developerLog.debug("Entering into com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || Method Name : kafkaProducerNotifyComponentUpdation() ||");
			
				  developerLog.debug("ComponentId: "+elementInstanceId);
			      developerLog.debug("Service Index for appasense:"+kafkaElasticProducer);
			      developerLog.debug("Procedure Service URL:"+kafkaElasticProducer);
			      
			      try {
			    	  String param =  "{\"client\":\"" +env.getProperty(client).trim()+ "\",\"elementInstanceId\":\"" +elementInstanceId+ "\"}";
			    	  developerLog.debug("param :  "+param);
			    	  byte[] postDataBytes= param.toString().getBytes("UTF-8");
			    	  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			    	  Date date = new Date();
			    	  developerLog.debug("Request time is "+dateFormat.format(date));
			    	      URL url = new URL(kafkaElasticProducer);
				          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				          conn.setDoOutput(true);
				          conn.setDoInput(true);	
				          conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				          conn.setRequestProperty("Content-Type", "application/json;charset=utf8");
				          conn.setRequestMethod("POST");
				          conn.setRequestProperty("Accept", "application/json");
				      	  conn.setRequestProperty("reqtime", dateFormat.format(date));
				      	 // conn.setConnectTimeout(Integer.parseInt(reqTimeOut));
				      	 // conn.setReadTimeout(Integer.parseInt(reqTimeOut));

	
				          OutputStream os = conn.getOutputStream();
				          os.write(postDataBytes);
				          os.flush();
				          os.close();
				          
				          if (conn.getResponseCode() != 200) {
				              throw new RuntimeException("Failed : HTTP error code : "
				                      + conn.getResponseCode());
				          }
	
				          BufferedReader br = new BufferedReader(new InputStreamReader(
				                  (conn.getInputStream())));
	
				          String output="";
				          StringBuilder sb = new StringBuilder();
				          while ((output = br.readLine()) != null) {
				        	  sb.append(output);
				            
				          }
				          developerLog.debug("#####Response from kafka procedure URL:"+sb.toString());
				          conn.disconnect();
	                      br.close();
		} catch (Exception e) {
			developerLog.debug("Exception in com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || kafkaProducerNotifyComponentUpdation()"+e);
		} finally {
		  developerLog.debug("Exiting from com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || Method Name : kafkaProducerNotifyComponentUpdation() ||");
		}
    return "success"; 
 }
	
	public String kafkaProducerNotifyComponentBulkUpdation(String user,String client,String elementChildId[])  {
	      MDC.put("category","com.dci.rest.bdo.elastic");
		  developerLog.debug("Entering into com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || Method Name : kafkaProducerNotifyComponentBulkUpdation() ||");
	
		  developerLog.debug("ComponentId: "+elementChildId);
	      developerLog.debug("Service Index for appasense:"+kafkaElasticProducer);
	      developerLog.debug("Procedure Service URL:"+kafkaElasticProducer);
	      try {
	    	  for (int i = 0; i < elementChildId.length; i++) {
	    		  String param =  "{\"client\":\"" +env.getProperty(client).trim()+ "\",\"elementInstanceId\":\"" +elementChildId[i]+ "\"}";
			    	  developerLog.debug("param :  "+param);
			    	  byte[] postDataBytes= param.toString().getBytes("UTF-8");
			    	  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			    	  Date date = new Date();
			    	  developerLog.debug("Request time is "+dateFormat.format(date));
		    	      URL url = new URL(kafkaElasticProducer);
			          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			          conn.setDoOutput(true);
			          conn.setDoInput(true);	
			          conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			          conn.setRequestProperty("Content-Type", "application/json;charset=utf8");
			          conn.setRequestMethod("POST");
			          conn.setRequestProperty("Accept", "application/json");
			      	  conn.setRequestProperty("reqtime", dateFormat.format(date));
			      	  //conn.setConnectTimeout(Integer.parseInt(reqTimeOut));
			      	  //conn.setReadTimeout(Integer.parseInt(reqTimeOut));
	
			          OutputStream os = conn.getOutputStream();
			          os.write(postDataBytes);
			          os.flush();
			          os.close();
			          
			          if (conn.getResponseCode() != 200) {
			              throw new RuntimeException("Failed : HTTP error code : "
			                      + conn.getResponseCode());
			          }
	
			          BufferedReader br = new BufferedReader(new InputStreamReader(
			                  (conn.getInputStream())));
	
			          String output="";
			          StringBuilder sb = new StringBuilder();
			          while ((output = br.readLine()) != null) {
			        	  sb.append(output);
			            
			          }
			          developerLog.debug("#####Response from kafka procedure URL:"+sb.toString());
			          conn.disconnect();
	                br.close();
	    	  }
	} catch (Exception e) {
		developerLog.debug("Exception in com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || kafkaProducerNotifyComponentBulkUpdation() "+e);
	} finally {
		developerLog.debug("Exiting from com.dci.rest.bdo.elastic.ElasticServerKafkaDAO || Method Name : kafkaProducerNotifyComponentBulkUpdation() ||");
	}
	return "success"; 
	}
	
}