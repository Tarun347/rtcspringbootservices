package com.dci.rest.dao.elastic;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class ElasticServiceAccess {
	private Logger developerLog = Logger.getLogger(ElasticServiceAccess.class);
	@Value("${ELASTIC_SERVER}" ) String  elServer;
	//@Value("${ELASTIC_SERVER_INDEX}" ) String  index;
	@Value("${RESULT_COUNT}" ) String  count;
	@Value("${ELASTIC_SEARCH_FLAG}" ) String elasticServiceFlag;
	@Value("${ELASTIC_STAT_SERVER}" ) String elStatServer;
	@Value("${REQUEST_TIME_OUT}" ) String reqTimeOut;
	@Value("${ELASTIC_SERVER_APPASENSE}" ) String appasense;
	
	protected HttpURLConnection getURLConnection(String serviceURL) {
		developerLog.debug("Sevice URL-->"+serviceURL);
		URL url;
		HttpURLConnection con = null;
		try {
			System.setProperty("https.protocols", "TLSv1.1");
			url = new URL(serviceURL);
			con = (HttpURLConnection) url.openConnection();
		} catch (Exception e) {			
			e.printStackTrace();
			return con;
		}
		return con;
	}
}
