package com.dci.rest.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.common.DciCommon;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.AppaSlide;
import com.dci.rest.model.AppaSlideDualDetail;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.CommentsBean;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ComponentStatus;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.DocumentType;
import com.dci.rest.model.Filter;
import com.dci.rest.model.FootNoteAttributes;
import com.dci.rest.model.Fund;
import com.dci.rest.model.Locale;
import com.dci.rest.model.Order;
import com.dci.rest.model.Placement;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.UserBean;
import com.dci.rest.model.SelectBean;
import com.dci.rest.model.ShareClass;
import com.dci.rest.model.Style;
import com.dci.rest.model.StyleBean;
import com.dci.rest.model.TableComponent;
import com.dci.rest.model.TableType;
import com.dci.rest.model.Tag;
import com.dci.rest.model.VariableBean;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DateFormat;
import com.dci.rest.utils.StringUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import springfox.documentation.spring.web.json.Json;

@Component
@Repository
public class AppaSlidesDAO extends DataAccessObject {

	private Logger developerLog = Logger.getLogger(ComponentDAO.class);

	@Value("${APP_ID}")
	private String appId;

	/**
	 * @Purpose:This method is used to get the list of context from the database.
	 * @return Collection
	 */

	public ComponentBean getComponentTestData(String user, String client, String componentName) {
		MDC.put("category", "com.dci.rest.dao.AppaSlidesDAO");
		developerLog.debug("Entering into com.dci.rest.dao.AppaSlidesDAO || Method Name : getComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentBean component = new ComponentBean();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug(
					"call SP8_RTC_SEARCHCOMPONENTSBYNAMES ('" + user + "'," + client + "," + componentName + ")");
			cs = con.prepareCall("{call SP8_RTC_SEARCHCOMPONENTSBYNAMES(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, componentName);
			rs = cs.executeQuery();
			while (rs.next()) {
				component.setElementInstanceId(rs.getString("felementinstanceid"));
				component.setType(rs.getString("felement_id"));
				component.setTableType(rs.getString("FTABLETYPE"));
				component.setName(rs.getString("fqualdata_desc"));
				component.setEffectiveDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				component.setExpirationDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				component.setBody(rs.getString("felement_detail"));
				component.setCreatedBy(rs.getString("fcreatedby"));
				component.setLastUpdatedBy(rs.getString("flastchangedby"));
				component.setStatusId(rs.getString("fcompstatus"));
				component.setStatusDesc(rs.getString("fcompstatus_desc"));
				component.setLocale(rs.getString("flanguageid"));
				component.setLocaleDesc(rs.getString("flanguage_desc"));
				component.setContext(rs.getString("felementcontextid"));
				component.setCreatedTime(DateFormat.expandedDate(rs.getDate("ftimecreated")));
				/*
				 * component.setQualDataTableType(rs.getString("fqualdata_tabletype"));
				 * component.setDocumentTypeAssociation(rs.getString("fbook_type"));
				 */
				component.setLastUpdatedTime(DateFormat.expandedDate(rs.getDate("ftimelastchanged")));
				component.setLocaleInd(rs.getString("FPRIMARYLINK"));
				component.setShadowId(rs.getString("fqualdatashadowid"));
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponent()", e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getComponent() ||");
		}
		return component;

	}

	public List<ContextBean> getElementContextList(String userId, String clientId, String objectId) {
		MDC.put("category", "com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getElementContextList() ||");
		ArrayList<ContextBean> elementContextList = new ArrayList<ContextBean>();
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog
					.debug("{CALL SP_RTC_ITGetElementContextList(" + userId + "," + clientId + "," + objectId + ")}");
			cs = con.prepareCall("{CALL SP_RTC_ITGetElementContextList(?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(objectId));
			rs = cs.executeQuery();
			while (rs.next()) {
				ContextBean elementContext = new ContextBean();
				elementContext.setId(rs.getInt("fELementContextId"));
				elementContext.setName(rs.getString("FELEMENTCONTEXT_DESC"));
				elementContextList.add(elementContext);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getElementContextList()", e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getElementContextList()");
		}
		return elementContextList;
	}

	public List<AppaSlideDualDetail> saveAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {

		developerLog.debug("Entering the appaSlidesDAO.saveAppaSlides || Method Name : saveAppaSlides() || input : ");

		String username = request.getParameter("name");
		String clientId = request.getParameter("clientid");

		String pptPath = requestBody.get(0).getpPTPath();
		String slideIDs = "";
		String returnIds = "";

		ArrayList<AppaSlideDualDetail> appaSlideDetailList = new ArrayList<AppaSlideDualDetail>();

		for (AppaSlide appaSlide : requestBody) {
			if (slideIDs.isEmpty()) {
				slideIDs = appaSlide.getLocalSlideId() + "@" + appaSlide.getSlideImageChecksum();
			} else {
				slideIDs = slideIDs + "," + appaSlide.getLocalSlideId() + "@" + appaSlide.getSlideImageChecksum();
			}
		}

		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			if (requestBody.isEmpty()) {
				// elementInstanceId = "0";
			}

			developerLog.debug("call SP8_PUTAPPAPPTSLIDES('" + username + "'," + clientId + ",'" + pptPath + "','"
					+ slideIDs + "')");
			cs = con.prepareCall("{call SP8_PUTAPPAPPTSLIDES(?,?,?,?,?)}");
			cs.setString(1, username);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3, pptPath);
			cs.setString(4, slideIDs);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			returnIds = cs.getString(5);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || saveAppaSlides()", e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.AppaSlidesDAO || Method Name : saveAppaSlides()");
		}
		List<String> listreturn = Arrays.asList(returnIds.trim().split(","));
		developerLog.debug("List Values: " + listreturn.toString());
		AppaSlideDualDetail appaSlideDetail = null;
		for (String id : listreturn) {
			developerLog.debug("ID value: " + id.toString());
			List<String> tempList = Arrays.asList(id.trim().split("@"));
			if (tempList.isEmpty())
				continue;
			developerLog.debug("TempList initial value: " + tempList.toString());
			if (!tempList.get(0).isEmpty()) {
				appaSlideDetail = new AppaSlideDualDetail();
				appaSlideDetail.setAppaSlideId(tempList.get(0));
				developerLog.debug("Getting first value " + tempList.get(0));
				appaSlideDetail.setSlideChecksum(tempList.get(1));
				developerLog.debug("Getting second value " + tempList.get(1));
				appaSlideDetailList.add(appaSlideDetail);
				developerLog.debug("The final List contains: " + appaSlideDetailList.toString());
				developerLog.debug("TempList count is: " + tempList.size());
				tempList = null;
				developerLog.debug(tempList);
			}

		}
		developerLog.debug("Exiting AppaSlidesDAO");
		developerLog.debug("The total values that the object stores is:" + appaSlideDetailList.size());
		developerLog.debug("After the loops" + appaSlideDetailList.toString());
		return appaSlideDetailList;
	}

	
	public List<AppaSlideDualDetail> checkAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {
		developerLog.debug("Entering the appaSlidesDAO.saveAppaSlides || Method Name : saveAppaSlides()");

		String username = request.getParameter("name");
		String clientId = request.getParameter("clientid");

		Map<String, String> slideDetailMap = new HashMap<String, String>();

		ArrayList<String> appaPathList = new ArrayList<String>();
		ArrayList<String> appaSlideIdList = new ArrayList<String>();

		ArrayList<AppaSlideDualDetail> appaSlideDetailList = new ArrayList<AppaSlideDualDetail>();

		for (AppaSlide appaSlide : requestBody) {
			if (slideDetailMap.containsKey(appaSlide.getpPTPath())) {
				String oldValue = slideDetailMap.get(appaSlide.getpPTPath());
				slideDetailMap.replace(appaSlide.getpPTPath(), oldValue + "," + appaSlide.getSlideId());
			} else {
				slideDetailMap.put(appaSlide.getpPTPath(), appaSlide.getSlideId());
			}
		}

		for (Map.Entry<String, String> entry : slideDetailMap.entrySet()) {
			appaPathList.add(entry.getKey());
			appaSlideIdList.add(entry.getValue());
		}

		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		for (int i = 0; i < appaPathList.size(); i++) {
			try {
				if (con == null || con.isClosed()) {
					con = getConnection();
				}
				
				developerLog.debug("call SP8_GETAPPAPPTSLIDES('" + username + "'," + clientId + ",'" + appaPathList.get(i) + "','"
						+ appaSlideIdList.get(i) + "')");
				cs = con.prepareCall("{call SP8_GETAPPAPPTSLIDES(?,?,?,?)}");
				cs.setString(1, username);
				cs.setInt(2, new Integer(clientId).intValue());
				cs.setString(3, appaPathList.get(i));
				cs.setString(4, appaSlideIdList.get(i));	
				rs = cs.executeQuery();
				while (rs.next()) {
					AppaSlideDualDetail appaSlideDetailFetched = new AppaSlideDualDetail();
					appaSlideDetailFetched.setAppaSlideId(rs.getString("FPPTSLIDESID"));;
					appaSlideDetailFetched.setSlideChecksum(rs.getString("FSLIDESCHECKSUM"));
					appaSlideDetailList.add(appaSlideDetailFetched);
				}

			} catch (Exception e) {
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || saveAppaSlides()", e);
			} finally {
				releaseConStmts(rs, cs, con, null);
				developerLog.debug("Exiting from com.dci.rest.dao.AppaSlidesDAO || Method Name : checkAppaSlides()");
			}

		}

		developerLog.debug("Exiting AppaSlidesDAO");
		developerLog.debug("The total values that the object stores is:" + appaSlideDetailList.size());
		developerLog.debug("After the loops" + appaSlideDetailList.toString());
		return appaSlideDetailList;
	}

}
