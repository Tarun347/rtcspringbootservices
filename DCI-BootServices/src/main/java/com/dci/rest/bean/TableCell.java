
package com.dci.rest.bean;
import java.util.*;

public class TableCell {
	private String cellvalue = null;
	private ArrayList attribute = null;
	private String cspan = null;
	private String isXBRLTaged="n";
	private String xbrlTag="";
	
	/**
	 * @return
	 */
	
	public TableCell() {
	}

	/**
	 * @return
	 */
	public String getIsxbrlTaged() {
		return isXBRLTaged;
	}
	public String getXbrlTag() {
		return xbrlTag;
	}
	/**
	 * @param string
	 */
	public void setIsxbrlTaged(String strTag) {
		isXBRLTaged = strTag;
	}
	public void setXbrlTag(String strTag) {
		xbrlTag = strTag;
	}
	
	public TableCell(String cellvalue, ArrayList attributes) {
		this.cellvalue = cellvalue;
		this.attribute = attributes;
	}
	
	public ArrayList getAttribute() {
		return attribute;
	}

	/**
	 * @return
	 */
	public String getCellvalue() {
		return cellvalue;
	}

	/**
	 * @param ArrayList
	 */
	public void setAttributes(ArrayList collection) {
		attribute = collection;
	}

	/**
	 * @param string
	 */
	
	public void setCellvalue(String string) {
		cellvalue = string;
	}
	
	/*
	 * To return the string form of collection attribute.
	 */
	public String getAttributes() {
		StringBuffer sb = new StringBuffer(" ");
		if (attribute == null) return "";		
		for(int i=0;i<attribute.size();i++) {			
			FieldAttribute fa = (FieldAttribute)attribute.get(i);
			if (fa != null) {
				sb.append(fa.getName());
				sb.append("=\"");
				sb.append(fa.getValue());
				sb.append("\" ");
			}
		} 
		return sb.toString();
	}
	/**
	 * @return
	 */
	public String getCspan() {
		return cspan;
	}

	/**
	 * @param string
	 */
	public void setCspan(String string) {
		cspan = string;
	}
}
