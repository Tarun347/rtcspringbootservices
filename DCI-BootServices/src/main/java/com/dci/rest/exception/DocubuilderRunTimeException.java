package com.dci.rest.exception;

public class DocubuilderRunTimeException extends DocubuilderException{
	
	String msg = "|| Exception Details ||"+"\n"+"|| RuntimeException in Module Name: ";
	
	public DocubuilderRunTimeException(Throwable ex, String module) {
		
		if(ex instanceof ArithmeticException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ArrayStoreException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ClassCastException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IllegalArgumentException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IllegalMonitorStateException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IllegalStateException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IndexOutOfBoundsException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof NegativeArraySizeException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof NullPointerException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof SecurityException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof UnsupportedOperationException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof NumberFormatException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IllegalThreadStateException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ArrayIndexOutOfBoundsException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof StringIndexOutOfBoundsException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else{
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}
	
	}

	public String getMsg(){
		return msg;
	}

}
