package com.dci.rest.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/*import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;*/

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Docubuilder API", "Docubuilder Spring-Boot library api", "1.0",
			"urn:tos", "Rohit", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
			
   
	@Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO)
                .select().apis(RequestHandlerSelectors.basePackage("com.dci.rest.controller"))
                .paths(regex("/.*"))
                .build().globalOperationParameters(getGlobalParameters());
             
    }
	
	private List<Parameter> getGlobalParameters() {
        List<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(
            new ParameterBuilder()
            .name("user")
            .description("UerName")
            .modelRef(new ModelRef("string"))
            .parameterType("query")
            .required(false)
            .build()
        );
        parameters.add(
	            new ParameterBuilder()
	            .name("client")
	            .description("ClientId")
	            .modelRef(new ModelRef("string"))
	            .parameterType("query")
	            .required(false)
	            .build()
	        );
        return parameters;
    }
}
