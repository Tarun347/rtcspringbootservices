package com.dci.rest.model;

import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Component
public class AppaSlide {
	
	@JsonProperty("LocalSlideId")
	private String localSlideId;
	
	@JsonProperty("PPTPath")
	private String pPTPath;
	
	@JsonProperty("SlideImageChecksum")
	private String slideImageChecksum;
	
	@JsonProperty("SlideImageStream")
	private String slideImageStream;
	
	@JsonProperty("SlideID")
	private String slideId;
	
	public String getSlideId() {
		return slideId;
	}
	public void setSlideId(String slideId) {
		this.slideId = slideId;
	}
	public String getLocalSlideId() {
		return localSlideId;
	}
	public void setLocalSlideId(String localSlideId) {
		this.localSlideId = localSlideId;
	}
	public String getpPTPath() {
		return pPTPath;
	}
	public void setpPTPath(String pPTPath) {
		this.pPTPath = pPTPath;
	}
	public String getSlideImageChecksum() {
		return slideImageChecksum;
	}
	public void setSlideImageChecksum(String slideImageChecksum) {
		this.slideImageChecksum = slideImageChecksum;
	}
	public String getSlideImageStream() {
		return slideImageStream;
	}
	public void setSlideImageStream(String slideImageStream) {
		this.slideImageStream = slideImageStream;
	}

	public String toString()
	{
		return this.localSlideId;
	}
}
