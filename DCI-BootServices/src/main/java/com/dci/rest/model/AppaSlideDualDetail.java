package com.dci.rest.model;

public class AppaSlideDualDetail {

	private String appaSlideId;
	private String slideChecksum;
	
	public String getAppaSlideId() {
		return appaSlideId;
	}
	public void setAppaSlideId(String appaSlideId) {
		this.appaSlideId = appaSlideId;
	}
	public String getSlideChecksum() {
		return slideChecksum;
	}
	public void setSlideChecksum(String slideChecksum) {
		this.slideChecksum = slideChecksum;
	}
}
