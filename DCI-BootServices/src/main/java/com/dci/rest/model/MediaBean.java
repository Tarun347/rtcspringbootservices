package com.dci.rest.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MediaBean {
	
	private String name;
	private String description;
	private String alt;
	private String id;
	private String date;
	private String size;
	private String createdBy;
	private String updatedBy;
	private String type;
	private String status;
	private String message;
	private String path;
	
	private String createTime;
	private String updateTime;

	private String documentTypeAssociation;
	private String assetClassAssociation;
	private String fundsAssociation;
	private String documentAssociation;

	
	private String tagId;
	private String tagName;
	
	private List<Tag> tags;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTagId() {
		return tagId;
	}

	public String getDocumentTypeAssociation() {
		return documentTypeAssociation;
	}

	public void setDocumentTypeAssociation(String documentTypeAssociation) {
		this.documentTypeAssociation = documentTypeAssociation;
	}

	public String getAssetClassAssociation() {
		return assetClassAssociation;
	}

	public void setAssetClassAssociation(String assetClassAssociation) {
		this.assetClassAssociation = assetClassAssociation;
	}

	public String getFundsAssociation() {
		return fundsAssociation;
	}

	public void setFundsAssociation(String fundsAssociation) {
		this.fundsAssociation = fundsAssociation;
	}

	public String getDocumentAssociation() {
		return documentAssociation;
	}

	public void setDocumentAssociation(String documentAssociation) {
		this.documentAssociation = documentAssociation;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}
