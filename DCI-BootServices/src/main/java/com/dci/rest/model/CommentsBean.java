package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class CommentsBean {
	
	
	private String comment;
	private String createdby;
	private String createdtime;
	private int commentcount;
	private String change;
	private int entityinstanceid;
	private int entityinstanceversion;
	private int entityinstanceshadowid;
	private String shadowId;
	private String display;
	private UserBean user;
	
	public UserBean getUser() {
		return user;
	}
	public void setUser(UserBean user) {
		this.user = user;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
	public int getCommentcount() {
		return commentcount;
	}
	public void setCommentcount(int commentcount) {
		this.commentcount = commentcount;
	}
	public String getChange() {
		return change;
	}
	public void setChange(String change) {
		this.change = change;
	}
	public int getEntityinstanceid() {
		return entityinstanceid;
	}
	public void setEntityinstanceid(int entityinstanceid) {
		this.entityinstanceid = entityinstanceid;
	}
	public int getEntityinstanceversion() {
		return entityinstanceversion;
	}
	public void setEntityinstanceversion(int entityinstanceversion) {
		this.entityinstanceversion = entityinstanceversion;
	}
	public int getEntityinstanceshadowid() {
		return entityinstanceshadowid;
	}
	public void setEntityinstanceshadowid(int entityinstanceshadowid) {
		this.entityinstanceshadowid = entityinstanceshadowid;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getShadowId() {
		return shadowId;
	}
	public void setShadowId(String shadowId) {
		this.shadowId = shadowId;
	}	

}
