package com.dci.rest.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Schema {

	private int id;	
	private int sId;	
	private String name;
	private String description;
	private String type;
	private String fStatus;
	private String fStatusDesc;
	private String fStatusName;
	private String fStatusAssigee;
	private String fOrder;
	private String statusType;
	private String assetClassId;
	private String assetClassName;
	private String creator;
	private List<WorkFLStatus> statusList;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssetClassId() {
		return assetClassId;
	}
	public void setAssetClassId(String assetClassId) {
		this.assetClassId = assetClassId;
	}
	public String getAssetClassName() {
		return assetClassName;
	}
	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	public String getfStatus() {
		return fStatus;
	}
	public void setfStatus(String fStatus) {
		this.fStatus = fStatus;
	}
	public String getfStatusDesc() {
		return fStatusDesc;
	}
	public void setfStatusDesc(String fStatusDesc) {
		this.fStatusDesc = fStatusDesc;
	}
	public String getfOrder() {
		return fOrder;
	}
	public void setfOrder(String fOrder) {
		this.fOrder = fOrder;
	}
	public List<WorkFLStatus> getStatusList() {
		return statusList;
	}
	public void setStatusList(List<WorkFLStatus> statusList) {
		this.statusList = statusList;
	}
	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId = sId;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getfStatusName() {
		return fStatusName;
	}
	public void setfStatusName(String fStatusName) {
		this.fStatusName = fStatusName;
	}
	public String getfStatusAssigee() {
		return fStatusAssigee;
	}
	public void setfStatusAssigee(String fStatusAssigee) {
		this.fStatusAssigee = fStatusAssigee;
	}


}
