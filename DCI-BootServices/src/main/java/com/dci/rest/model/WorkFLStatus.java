package com.dci.rest.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class WorkFLStatus {
	
	private int order;	
	private int id;	
	private int sId;
	private String name;
	private String description;
	private String assignee;
	private int type;
	private String transitionIds;
	private String rules;
	
	private List<Transition> transition;
	
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId = sId;
	}
	public void setTransition(List<Transition> transition) {
		this.transition = transition;
	}
	public List<Transition> getTransition() {
		return transition;
	}
	public String getTransitionIds() {
		return transitionIds;
	}
	public void setTransitionIds(String transitionIds) {
		this.transitionIds = transitionIds;
	}

	
}
