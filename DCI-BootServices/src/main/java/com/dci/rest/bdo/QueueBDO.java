package com.dci.rest.bdo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.CommonDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.QueueBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.utils.DateFormat;

@Component
public class QueueBDO extends ComponentCommons{
	
	@Autowired
	CommonDAO commonDAO;
	
	private Logger developerLog = Logger.getLogger(QueueBDO.class);
	
	public Response<Object> getQueuedProofsList(HttpServletRequest request) {
	    MDC.put("category","com.dci.rest.bdo.QueueBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.QueueBDO || Method Name : getQueuedProofs() ||");                
	 	List<QueueBean> queuedProofsList = new ArrayList<QueueBean>();
	     try{
			    if(!validateClientDetails(request)) {
			    	return validClientDetailsError();
			    }
			 String userName = request.getParameter("user").toString();
			 String client = request.getParameter("client").toString();
     		 queuedProofsList = commonDAO.getQueuedProofsList(userName,client);	
     		return response(200,"SUCCESS",null,queuedProofsList);  		
        }catch(Exception e){
        	new DocubuilderException("Exception in com.dci.rest.bdo.QueueBDO || getQueuedProofsList()", e);
        	return response(500,"ERROR",internalServerError,null);
        }
	}
	
	@SuppressWarnings("deprecation")
	public Response<Object> getQueuedChartList(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.QueueBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.QueueBDO || Method Name : getQueuedProofs() ||");
		String documentId = null;
		List<QueueBean> queuedProofsList = new ArrayList<QueueBean>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			Date todayDate = new Date();
			int year = todayDate.getYear() + 1900;
			String todaysDate = todayDate.getMonth() + 1 + "/" + todayDate.getDate() + "/" + year;
			queuedProofsList = commonDAO.getQueuedChartList(userName, client, documentId, todaysDate, todaysDate);
			return response(200, "SUCCESS", null, queuedProofsList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.QueueBDO || getQueuedProofs()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> getCycleQueueDocuments(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.QueueBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.QueueBDO || Method Name : getCycleQueueDocuments() ||");
		List<QueueBean> cycleQueueList = new ArrayList<QueueBean>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			cycleQueueList = commonDAO.getCycleQueueDocuments(userName, client);
			return response(200, "SUCCESS", null, cycleQueueList);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.QueueBDO || getCycleQueueDocuments()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> getUploadQueue(HttpServletRequest request)  {
	    MDC.put("category","com.dci.rest.bdo.QueueBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.QueueBDO || Method Name : getUploadQueue() ||");                
	 	List<QueueBean> uploadDataFileList = new ArrayList<QueueBean>();
       try{
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			uploadDataFileList = commonDAO.getImportUploadQueueList(userName, client, DateFormat.getCurrentDateMDY(), DateFormat.getCurrentDateMDY());
			return response(200,"SUCCESS",null,uploadDataFileList);
        }catch(Exception e){
        	new DocubuilderException("Exception in com.dci.rest.bdo.QueueBDO || getUploadQueue()", e);
        	return response(500,"ERROR",internalServerError,null);
        }
	}

}
