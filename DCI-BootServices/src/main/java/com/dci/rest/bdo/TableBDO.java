package com.dci.rest.bdo;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.xerces.dom.DocumentImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.bean.TableDetails;
import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.model.TableComponent;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.StringUtility;

@Service
public class TableBDO  extends ComponentCommons{

	private Logger developerLog = Logger.getLogger(TableBDO.class);
    
	@Autowired
	ComponentDAO compDAO;
	
	public Response<Object> createTableComponent(TableComponent table, BindingResult bindingResult,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.TableBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.TableBDO || Method Name : createTableComponent() ||");

		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();		
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult,table);
			}
			table.setOperation("create");
/*			if(! userPrevilege.checkComponentPerrmission(userName, clientId,table)) {
				status.setStatusCode(403);
				status.setStatus("ERROR");
				status.setMessage("Insufficient Previlege");
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(table);	
				return responseVOTemp;
			}*/
			String existingComponentResult = compDAO.checkComponentName(userName, clientId, null, table.getName());
	        if(existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")){
	        	status.setStatusCode(409);
	        	status.setStatus("ERROR");
	        	status.setMessage("Duplicate Component");
	        	responseVOTemp.setStatus(status);
	        	responseVOTemp.setResult(table);	
	        	return responseVOTemp; 
	        }
/*	        table.setBody(CheckString.convertXHtmlToDocBook(table.getBody()));
			body = CheckString.replaceStringX(body, "<table>","<table rows=\"" + rows + "\" cols=\""+ columns+ "\" render=\"0\" datatype=\"" + table.getDataType()+ "\" tabletype=\"" + table.getTableType()+ "\" primary=\"" + tableBean.getPrimary()+ "\" style=\"" + tableBean.getStyle() + "\">");
			if (CheckString.convertXHtmlToDocBook(body).length() >= 11500) {
				table.setQuantData(body);				
				table.setBody(body.substring(0, body.indexOf("<tbody>"))+ "<tbody><row><entry></entry></row></tbody></table>");
			}else {
				table.setBody(body);
			}	*/	

			if(CheckString.isValidString(table.getTableBody())) {
				//table.setBody(CheckString.convertXHtmlToDocBook(table.getBody()));
				table.setBody(doTableConversionOperations(table));
			}		

	        table.setGlobal("1");
			table.setQualDataCon("default");
			table.setFootnoteIds(getHiddenValues(table.getBody(), "footnote"));
			table.setRecursiveIds(getHiddenValues(table.getBody(), "embedded"));
			table.setEntityIds(getHiddenValues(table.getBody(), "userVariable"));
			table.setSystemEntityIds(getHiddenValues(table.getBody(), "systemVariable"));
			Map<String, String> objectNode = compDAO.createTableComponent(userName, clientId, table);
			compDAO.setComponentAssignee(userName,clientId,objectNode.get("elementId"),table);
			ComponentBDO componentBdo = new ComponentBDO();
			componentBdo.setComponentAssociation(userName, clientId, table);
        	if(table.getTagAssociation()!=null && table.getTagAssociation().size()>0) {
        		Map<String, String> tempParam = new HashMap<String, String>();
        		tempParam.put("user", userName);
        		tempParam.put("client", clientId);
        		tempParam.put("componentId", table.getElementInstanceId());
        		componentBdo.createTags(tempParam, table,request);
        	}
        	
        	if(objectNode.get("message").toString().equals("success")) {
        		status.setStatusCode(200);
        		status.setStatus("SUCCESS");
        		table.setElementInstanceId(objectNode.get("elementId"));
        		responseVOTemp.setStatus(status);
        		responseVOTemp.setResult(table);
        	}else if(objectNode.get("message").toString().equals("texttoolong")){
        		status.setStatusCode(413);
        		status.setStatus("ERROR");
        		status.setMessage("Text too long");
        		responseVOTemp.setStatus(status);
        		responseVOTemp.setResult(table);

        	}else if(objectNode.get("message").toString().equals("footnoteid already exist,error")){
        		status.setStatusCode(409);
        		status.setStatus("ERROR");
        		status.setMessage("Footnote id already exist");
        		responseVOTemp.setStatus(status);
        		responseVOTemp.setResult(table);
        	}else{
        		status.setStatusCode(500);
        		status.setStatus("ERROR");
        		responseVOTemp.setStatus(status);
        		responseVOTemp.setResult(table);
        	}
            	
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.TableBDO || createTableComponent()", e);
		}
		return responseVOTemp;
	}
	
	
	public Response<Object> editTableComponent(String idToEdit, ComponentBean componentObj,Map<String,Object> reqParams,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.TableBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.TableBDO || Method Name : editTableComponent() ||");

		Map<String, Object> updateProcessStatus = new HashMap<String, Object>();
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		ComponentBDO componentBdo = new ComponentBDO();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			componentObj.setOperation("edit");
/*			if(CheckString.isValidString(componentObj.getType()) && ! userPrevilege.checkComponentPerrmission(userName, clientId,componentObj )) {
				status.setStatusCode(403);
				status.setStatus("ERROR");
				status.setMessage("Insufficient Previlege");
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(componentObj);	
				return responseVOTemp;
			}*/
			componentObj.setElementInstanceId(idToEdit);
			ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());
			if(!CheckString.isValidString(componentObj.getType())) {
				componentObj.setType(compDataFromDB.getType());
			}
			//check if name is already exist
			if(CheckString.isValidString(componentObj.getName()) && CheckString.isValidString(componentObj.getElementInstanceId())) {
				String existingComponentResult = compDAO.checkComponentName(userName, clientId, componentObj.getElementInstanceId(), componentObj.getName());
				if(existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")){
					status.setStatusCode(409);
					status.setStatus("ERROR");
					status.setMessage("Duplicate Component");
					responseVOTemp.setStatus(status);
					responseVOTemp.setResult(componentObj);	
		        	return responseVOTemp; 
				}				
	        }
			if(CheckString.isNumericValue(componentObj.getElementInstanceId())){
				boolean isFieldUpdate = false;//this flag can be used for future reference where the legacy code has written to update the component inside the if block dao call
	    		if(CheckString.isValidString(componentObj.getName()) || CheckString.isValidString(componentObj.getBody()) || CheckString.isValidString(componentObj.getBody())|| CheckString.isValidString(componentObj.getLocale())|| CheckString.isValidString(componentObj.getContext())|| CheckString.isValidString(componentObj.getEffectiveDate())|| CheckString.isValidString(componentObj.getExpirationDate()) || CheckString.isValidString(componentObj.getTableType())|| CheckString.isValidString(componentObj.getFcomp_assignee())) {
	    			isFieldUpdate = true;
    			}
	    		if(componentObj.getTableBody() != null && !componentObj.getTableBody().trim().equals("")) {
	    			isFieldUpdate = true;
	    		}
	    		if(isFieldUpdate) {	
					//ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());
					componentObj.setQualData_Id(compDataFromDB.getQualData_Id());
					componentObj.setQualDataId(compDataFromDB.getQualDataId());
					componentObj.setLastUpdatedTime(compDataFromDB.getLastUpdatedTime());
					
					String contentChangedFlag = "false";				
					//convert component type  process
					if((CheckString.isValidString(componentObj.getConvertTo()) && !componentObj.getConvertTo().equalsIgnoreCase("-1")) && (componentObj.getType().equalsIgnoreCase("para")||componentObj.getType().equalsIgnoreCase("bridgehead"))){
						compDAO.convertComponent(userName, clientId, componentObj.getElementInstanceId(), compDataFromDB.getType(), componentObj.getConvertTo());
					}
					
					if(CheckString.isValidString(componentObj.getTableBody())) {
						componentObj.setBody(doTableConversionOperations(componentObj));//added to take the data in the old library way do the conversions accordingly
						updateProcessStatus.put("body", getConvertedBody(componentObj.getBody(),componentObj.getType()));
						//componentObj.setBody(CheckString.convertXHtmlToDocBook(componentObj.getBody()));
						componentObj.setFootnoteIds(getHiddenValues(componentObj.getBody(), "footnote"));
						componentObj.setRecursiveIds(getHiddenValues(componentObj.getBody(), "embedded"));
						componentObj.setEntityIds(getHiddenValues(componentObj.getBody(), "userVariable"));
						componentObj.setSystemEntityIds(getHiddenValues(componentObj.getBody(), "systemVariable"));
					}		
					componentObj.setGlobal("1");
					componentObj.setQualDataCon("default");	
					
		        	Map<String, Object> paramMap = new HashMap<String, Object>();
		        	paramMap.put("componentObj",componentObj);
		    		paramMap.put("overwriteStatus",(CheckString.isValidString(componentObj.getOverrideStatus())&&componentObj.getOverrideStatus().equalsIgnoreCase("y")) ? "Y" : "N");
		    		paramMap.put("qualDataCond",(CheckString.isValidString(componentObj.getQualDataCon())) ? componentObj.getQualDataCon() : "");
		    		paramMap.put("qualDataTableType",(CheckString.isValidString(componentObj.getQualDataTableType())) ? componentObj.getQualDataTableType() : "");
		    		if(CheckString.isValidString(componentObj.getBody()) && !compDataFromDB.getBody().equals(componentObj.getBody())){
		    			contentChangedFlag = "true";
		    		}
		    		paramMap.put("contentchanged",contentChangedFlag);
		        	
		    		//update component data    		    			
	    			updateProcessStatus.put("updateStatus", compDAO.editComponentField(userName, clientId, paramMap));
	    		}else {  			
	    			//updateProcessStatus.put("updateStatus", compDAO.editComponentData(userName, clientId, paramMap));for legacy code update	
	    			//set Association process
		    		if(CheckString.isValidString(componentObj.getDocumentAssociation()) || CheckString.isValidString(componentObj.getDocumentTypeAssociation()) || CheckString.isValidString(componentObj.getFundsAssociation()) || CheckString.isValidString(componentObj.getAssetClassAssociation())) {
		    			updateProcessStatus.put("associationStatus",componentBdo.setComponentAssociation(userName, clientId, componentObj));
		    		}	    		
		        	//set tag association
		    		if(CheckString.isValidString(componentObj.getTags())){
		    			updateProcessStatus.put("tagStatus",componentBdo.setTagAssociation(userName, clientId, componentObj));
		    		}
		    		//update footnote mapping
		    		if(componentObj.getType().equalsIgnoreCase("footnote")&& !componentObj.getFootnoteMapping().equals(componentObj.getPreviousFootnoteMapping())){
		    			updateProcessStatus.put("footnoteMappingStatus",componentBdo.processFootnoteMapping(userName, clientId, componentObj));
		    		}
	    		}			
	    		status.setStatusCode(200);
	    		status.setStatus("SUCCESS");
	    		responseVOTemp.setStatus(status);
	    		responseVOTemp.setResult(updateProcessStatus);	
			}else {
				status.setStatusCode(400);
				status.setStatus("ERROR");
				status.setMessage("Invalid Component ID");
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(idToEdit);	
			}
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.TableBDO || editTableComponent()", e);
		}		
        return responseVOTemp;
	}


	private String doTableConversionOperations(ComponentBean tableObj) throws Exception {
		String mergedCells = tableObj.getMergeCellInfo();
		String tableBodyString = tableObj.getTableBody();
		int rows = CheckString.getInt(tableObj.getRows());
		int cols = CheckString.getInt(tableObj.getColumns());
		String rowStyles = tableObj.getRowStyles() != null ? tableObj.getRowStyles()  : "";
		String prevRenderType = "0";//update later
		String tabletype = tableObj.getTableType();
		String tablename = tableObj.getName();
		String datatype = tableObj.getDataTypeInd();//update later
		checkTableForZeroRowZeroColumn(rows, cols, tablename, tableBodyString);
		TableDetails tb = new TableDetails(rows, cols);
		tb.setTotalcols(cols);
		tb.setTotalrows(rows);
		tb.setTableValue(rows, cols,tableBodyString);
		tb.setCellValues(tb, tableBodyString, mergedCells);
		tb.registerStyleAttributes(rowStyles.split("\\$8910"));
		tb.setGraphic(null);
/*		tb.setNewAttribute(getNewAttributeList());
		tb.setNewAddedColAttribute(getNewCollAtt());
		tb.setNewRowAttribute(getNewRowAttribList());
		tb.setNewAddedRowAttributes(getNewRowAtt());
		if(colCheckBox != null && rowCheckBox != null ) {
			tb.setSelcol(colCheckBox);
			tb.setSelrow(rowCheckBox);
		} else {
			tb.setSelcol(colCheckBox);
			tb.setSelrow(rowCheckBox);
		}*/
		String body = tb.getXHtmlTable();
		body = CheckString.replaceStringX(body, "<table>", "<table rows=\"" + tb.getTotalrows() + "\" cols=\"" + tb.getTotalcols() 
							+ "\" render=\""+prevRenderType+"\" datatype=\"" + datatype + "\" tabletype=\""
							+ tabletype + "\" primary=\""+ tb.getPrimary() + "\" style=\"" + tb.getStyle() + "\">");
		return body;
	}	
	
	private void checkTableForZeroRowZeroColumn(int totalRows, int totalCols, String tableName, String tableGridValueParam) throws Exception{
		String excMsg = "Table cannot have zero";
		if(totalRows == 0 ){
			if(tableName != null){
				excMsg = tableName+" "+excMsg+" rows";
			}else{
				excMsg = excMsg+" rows";
			}
			if(tableGridValueParam != null){
				excMsg = excMsg + "\n" + tableGridValueParam;
			}
			throw new Exception(excMsg);
		}
		if(totalCols == 0 ){
			if(tableName != null){
				excMsg = tableName+" "+excMsg+" cols";
			}else{
				excMsg = excMsg+" cols";
			}
			if(tableGridValueParam != null){
				excMsg = excMsg + "\n" + tableGridValueParam;
			}
			throw new Exception(excMsg);
		}
	}
	
	@SuppressWarnings("unused")
	private class TableInner extends DBXmlManager {
		private String getFootnotes(DocumentImpl doc, String symbol) {
			String footnotes = "";
			NodeList col = doc.getElementsByTagName(symbol);
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				if (i == 0)
					footnotes = footnotes + elem.getAttribute("value");
				else
					footnotes = footnotes + "*" + elem.getAttribute("value");
			}
			return footnotes;
		}

		private String getEntities(DocumentImpl doc, String symbol) {
			String entities = "";
			NodeList col = doc.getElementsByTagName(symbol);
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("entity") > -1
						&& !type.equalsIgnoreCase("sentity")) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private String getSystemEntities(DocumentImpl doc, String symbol) {
			String entities = "";
			NodeList col = doc.getElementsByTagName(symbol);
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("sentity") > -1) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private String getRecursive(DocumentImpl doc, String type) {
			String entities = "";
			NodeList col = doc.getElementsByTagName("img");
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				if (type.indexOf(elem.getAttribute("type")) > -1) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private DocumentImpl getXmlTable(String str) throws Exception {
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>");
			xmlString.append(StringUtility.substituteEntityName(str));
			return getDOMFromXMLString(xmlString.toString());
		}

		private String getMaxSysentitySeqId(DocumentImpl doc, String symbol) {
			int seqid =0;
			int rValue = CheckString.getInt(new TableInner().getMaxSysentitySeqId(doc,"img"));
			NodeList col = doc.getElementsByTagName(symbol);
			for (int i=0;i<col.getLength();i++) {
				Element elem = (Element)col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("sentity") > -1 && elem.getAttribute("sequenceid") != null)
					seqid = CheckString.getInt(elem.getAttribute("sequenceid"));
				if(rValue < seqid)	rValue = seqid;			
			}
			return ""+rValue;
		}
	}
	


}
