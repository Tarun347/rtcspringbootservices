package com.dci.rest.controller.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.APIModel;
import com.dci.rest.model.MediaBean;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.MediaService;

@RestController
@RequestMapping({"${MEDIA}"})
@CrossOrigin
public class MediaController {

	private Logger developerLog = Logger.getLogger(MediaController.class);

	@Autowired
	MediaService mediaService;

	
	@RequestMapping(value = "${CREATE_MEDIA}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createMedia(HttpServletRequest request,@RequestBody MediaBean media,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : createMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.createMedia(request,media,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : createMedia() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${REMOVE_MEDIA}", method = RequestMethod.DELETE)
	public Response<Object> removeMedia(HttpServletRequest request,@PathVariable Map<String, String> pathVariablesMap){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : createMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.removeMedia(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : createMedia() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${SET_MEDIA_TAG_ASSOCIATION}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createAssoMediaTag(HttpServletRequest request,@PathVariable Map<String, String> pathVariablesMap,@RequestBody MediaBean media,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : createMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.createAssoMedia(pathVariablesMap,request,media,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : createMedia() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${SET_MEDIA_ASSOCIATION}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> mediaAssociation(HttpServletRequest request,@PathVariable Map<String, String> pathVariablesMap,@RequestBody MediaBean media,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : createMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.mediaAssociation(pathVariablesMap,request,media,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : createMedia() || response : "+responseVO);
		return responseVO;		
	}
	
	@RequestMapping(value = "${GET_MEDIA_ASSOCIATION}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAssociationData(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAssociationData() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.getMediaAssociation(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getAssociationData() || response : "+responseVO);
		return  responseVO;
	}
	
	
	@RequestMapping(value = "${CREATE_MEDIA_API}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createMediaAPI(@PathVariable Map<String, String> pathVariablesMap,@RequestBody APIModel apiModel,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createMediaAPI() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.createMediaAPI(pathVariablesMap,apiModel,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createMediaAPI() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_MEDIA_ALL_API}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getMediaAPI(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getMediaAPI() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.getMediaAPI(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getMediaAPI() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${EDIT_MEDIA}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editMedia(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody MediaBean media,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : createMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.editMedia(pathVariablesMap,request,media,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : createMedia() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = {"${GET_ALL_MEDIA}","${GET_MEDIA}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAllMedia(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : getAllMedia() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.getAllMedia(request,pathVariablesMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : getAllMedia() || response : "+responseVO);
		return responseVO;		
	}
	
	@RequestMapping(value = "${DISSASSO_TAG}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> disAssociateMediaTag(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody MediaBean media,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : disAssociateMediaTag() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mediaService.disAssociateMediaTag(request,pathVariablesMap,media,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : disAssociateMediaTag() || response : "+responseVO);
		return responseVO;		
	}
}
