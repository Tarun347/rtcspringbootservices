package com.dci.rest.controller.library;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Mail;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.MailService;

@RestController
@RequestMapping({"${MAIL_MODULE}"})
@CrossOrigin
public class MailController {
	@Autowired MailService mailService;
	private Logger developerLog = Logger.getLogger(MailController.class);
	
	@RequestMapping(value="${SEND_MAIL}", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> sendMailController(@RequestBody Mail mail, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.MailController");
		developerLog.debug("Entering into com.dci.rest.controller.library.MailController || Method Name : sendMailController() || input : "+mail);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = mailService.sendMail(mail,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MailController || Method Name : sendMailController() || response : "+responseVO);
		return  responseVO;
	}
		

}
