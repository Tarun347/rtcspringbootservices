package com.dci.rest.controller.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
import com.dci.rest.service.library.WorkFlowService;

@RestController
@RequestMapping({"${SCHEMA_WORKFLOW}"})
@CrossOrigin
public class WorkFlowController {
	
	private Logger developerLog = Logger.getLogger(WorkFlowController.class);
	
	@Autowired
	WorkFlowService workFlowService;
	
	
	@RequestMapping(value = "${GET_SCHEMAS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAllSchemaList(HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : getAllSchemaList() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.getAllSchemaList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : getAllSchemaList() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_TRANSITIONS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getStatusTransiton(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : getStatusTransiton() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.getStatusTransiton(pathVariablesMap.get("statusId").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : getStatusTransiton() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_SCHEMA_DEATAILS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getSchemaDetails(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : getStatusTransiton() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.getSchemaDetails(pathVariablesMap.get("schemaId").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : getStatusTransiton() || response : "+responseVO);
		return  responseVO;
	}
	
	@RequestMapping(value = "${CREATE_SCHEMA}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createSchema(@RequestBody Schema schema,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : createSchema() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.createSchema(schema,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : createSchema() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${EDIT_SCHEMA}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editSchema(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody Schema schema,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : editSchema() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.editSchema(pathVariablesMap.get("schemaId").toString(),schema,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : editSchema() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${EDIT_SCHEMA_STATUS}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editSchemaStatus(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody Schema schema,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.WorkFlowController");
		developerLog.debug("Entering into com.dci.rest.controller.library.WorkFlowController || Method Name : editSchemaStatus() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = workFlowService.editSchemaStatus(pathVariablesMap.get("schemaId").toString(),schema,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.WorkFlowController || Method Name : editSchemaStatus() || response : "+responseVO);
		return  responseVO;
	}
}
