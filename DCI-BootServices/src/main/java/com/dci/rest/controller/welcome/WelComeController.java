package com.dci.rest.controller.welcome;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dci.rest.auth.service.AuthService;
import com.dci.rest.model.APIModel;

@Controller
@RequestMapping("/")
@CrossOrigin
public class WelComeController {
	
	@Autowired
	AuthService authService;
	
	private Logger developerLog = Logger.getLogger(WelComeController.class);

	@RequestMapping(value = "/",method = RequestMethod.GET)
	public String showWelcomePage(org.springframework.ui.Model model,APIModel aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		model.addAttribute("authUser", aPIBean);
		developerLog.warn("Entering into com.dci.rest.controller.welcome.WelComeController || Method Name : showWelcomePage() ||");
		return "home";
	}
	@RequestMapping(value = "/register",method = RequestMethod.POST)
	public String registerRestUser(@ModelAttribute("authUser") APIModel aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		developerLog.warn("Entering into com.dci.rest.controller.welcome.WelComeController || Method Name : registerRestUser() ||");
		System.out.println(aPIBean.getApp_URL());
		//authService.registerClient(aPIBean);
		return "welcome";
	}
	
	

}
