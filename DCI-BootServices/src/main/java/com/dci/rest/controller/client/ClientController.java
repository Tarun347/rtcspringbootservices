package com.dci.rest.controller.client;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Response;
import com.dci.rest.service.library.DepartmentService;

@RestController
@RequestMapping({"${CLIENT}"})
@CrossOrigin
public class ClientController {

	private Logger developerLog = Logger.getLogger(ClientController.class);
	
	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping(value = "${GET_DEPARTMENTS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  Response<Object> getDepartmentList(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.client");
		developerLog.debug("Entering into com.dci.rest.controller.client.ClientController || Method Name : getDepartmentList() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			   responseVO=departmentService.getDepartmentList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.client.ClientController || Method Name : getDepartmentList() || response : "+responseVO);
		return  responseVO;
		
	}
}
