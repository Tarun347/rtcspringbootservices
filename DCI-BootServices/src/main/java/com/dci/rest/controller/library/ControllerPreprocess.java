package com.dci.rest.controller.library;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Value;

public class ControllerPreprocess {
	@Value("${USERID}" ) private String userId;
	@Value("${CLIENTID}" ) private String clientId;
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	protected Map setUserDetails(Map reqParam) {		
		try {
			if(reqParam.size()==0 ) {
				reqParam = new HashMap();
			}
			if(!reqParam.containsKey("user") ) {
				reqParam.put("user", this.userId);
			}
			if(!reqParam.containsKey("client")) {
				reqParam.put("client", this.clientId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reqParam;
	}
	
	
	protected boolean validateClientDetails(HttpServletRequest request) {
		if(request.getParameter(("user"))!=null && request.getParameter("client")!=null){
			return true;
		}
		return false;
	}
}
