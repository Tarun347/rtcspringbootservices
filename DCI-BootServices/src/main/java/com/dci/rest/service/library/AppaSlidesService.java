package com.dci.rest.service.library;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;

import com.dci.rest.bdo.AppaSlidesBDO;
import com.dci.rest.bdo.ComponentBDO;
import com.dci.rest.bdo.TableBDO;
import com.dci.rest.controller.library.AppaSlidesController;
import com.dci.rest.model.AppaSlide;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.TableComponent;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class AppaSlidesService {
	
	@Autowired AppaSlidesBDO appaSlidesBDO;
	//@Autowired TableBDO tableBDO;
	private Logger developerLog = Logger.getLogger(AppaSlidesController.class);

	public Response<Object> saveAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {
		developerLog.debug("Entering the appaSlideService.saveAppaSlides || Method Name : saveAppaSlides() || input : ");
		developerLog.debug("Calling the appaSlidesBDO.saveAppaSlides || Method Name : saveAppaSlides() || input : ");
		return appaSlidesBDO.saveAppaSlides(request, requestBody);
	}

	public Response<Object> checkAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {
		developerLog.debug("Entering the appaSlideService.saveAppaSlides || Method Name : checkAppaSlides() || input : ");
		developerLog.debug("Calling the appaSlidesBDO.saveAppaSlides || Method Name : checkAppaSlides() || input : ");
		return appaSlidesBDO.checkAppaSlides(request, requestBody);
	}

}
