package com.dci.rest.service.library;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.bdo.WorkFlowBDO;
import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
@Service
public class WorkFlowService {

	@Autowired WorkFlowBDO workFlowBDO;
	
	public Response<Object> getAllSchemaList(HttpServletRequest request){
		return workFlowBDO.getAllSchemaList(request);
	}
	public Response<Object> getStatusTransiton(String schemaId,HttpServletRequest request){
		return workFlowBDO.getStatusTransiton(schemaId,request);
	}
	public Response<Object> getSchemaDetails(String schemaId,HttpServletRequest request){
		return workFlowBDO.getSchemaDetails(schemaId,request);
	}
	public Response<Object> createSchema(Schema schema,HttpServletRequest request){
		return workFlowBDO.createSchema(schema,request);
	}
	public Response<Object> editSchema(String schemaId,Schema schema,HttpServletRequest request){
		return workFlowBDO.editSchema(schemaId,schema,request);
	}
	public Response<Object> editSchemaStatus(String schemaId,Schema schema,HttpServletRequest request){
		return workFlowBDO.editSchemaStatus(schemaId,schema,request);
	}
	
}
